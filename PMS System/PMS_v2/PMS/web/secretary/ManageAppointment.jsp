<%-- 
    Document   : ManageAppointment
    Created on : 2020年1月15日, 下午03:00:09
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Appointment</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            
            input[type=date], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            
            input[type=time], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            .navbar {
                overflow: hidden;
                background-color: #7892c2;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover {
                background-color: #476e9e;
            }

            .btn_submit {
                display: inline-block;
                padding: 5px 60px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;   
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #7892c2;
                border: none;
                border-radius: 15px;
                box-shadow: 0 9px #999;
            }

            .btn_submit:hover {background-color: #476e9e}

            .btn_submit:active {
                background-color: #476e9e;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }
            
            th {
                background-color: #5a97e8;
                color: #fff;
                border-bottom-width: 0;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC">
        <div class="navbar">
            <a href="./ManageAppointment.jsp" >Create appointment</a>
            <a href="./RemovePatients.jsp" >Remove patients</a>
            <a href="../" >Logout</a>
        </div>
        <br>
        <div>
            <form name="manage_appointment_from" action="../ManageAppointmentServlet" method="post" target="_self">
                <table border="1" align="center">
                    <tr>
                        <th align="center" colspan="2" style="background-color: #5b6cff;">
                            <h3>Make appointment for Patient</h3>
                        </th>
                    </tr>
                    <tr>
                        <th>Patient ID</th>
                        <td><input type="text" name="patientID" /></td>
                    </tr>
                    <tr>
                        <th>Doctor ID</th>
                        <td><input type="text" name="doctorID" /></td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td><input type="date" name="date" /></td>
                    </tr>
                    <tr>
                        <th>Time</th>
                        <td><input type="time" name="time" /></td>
                    </tr>
                </table>
                <br>
                <table style="margin:0 auto">
                    <tr>
                        <td align="center" colspan="2"><input class="btn_submit" type="submit" value="Submit"></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>

