<%-- 
    Document   : ViewAppointments
    Created on : 2020年1月15日, 下午02:58:54
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert title here</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            
            .navbar {
                overflow: hidden;
                background-color: #7892c2;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover {
                background-color: #476e9e;
            }
            
            .btn_view {
                display: inline-block;
                padding: 5px 60px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;   
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #7892c2;
                border: none;
                border-radius: 15px;
                box-shadow: 0 9px #999;
            }

            .btn_view:hover {background-color: #476e9e}

            .btn_view:active {
                background-color: #476e9e;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }

            th {
                background-color: #5a97e8;
                color: #fff;
                border-bottom-width: 0;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC">
        <div class="navbar">
            <a href="/PMS/doctor/ViewAppointments.jsp" >Appointments</a>
            <a href="/PMS/doctor/InspectPatientHistory.jsp">Patient history</a>
            <a href="/PMS/doctor/PrescribeMedicinesAndDosages.jsp">Medicines</a>
            <a href="/PMS/" >Logout</a>
        </div>
        <br>
        <div>
            <form name="doctor_appointment_from" action="../ViewDoctorAppointmentServlet" method="post">
                <table border="1" align="center">
                    <tr>
                        <th align="center" colspan="4" style="background-color: #5b6cff;">
                            <h3>View Appointments</h3>
                        </th>
                    </tr>
                    <tr>
                        <th>Doctor's ID</th>
                        <td colspan="3"><input type="text" name="id" /></td>
                    </tr>
                    <tr>
                        <th><b>Patient ID</b></th>
                        <th><b>Doctor ID</b></th>
                        <th><b>Start Date</b></th>
                        <th><b>Start Time</b></th>
                    </tr>
                    <c:forEach items="${requestScope.list}" var="appointment">
                        <tr align="center" bgcolor="white">
                            <td>${appointment.getPatientid()}</td>
                            <td>${appointment.getDoctorid()}</td>
                            <td>${appointment.getDate()}</td>
                            <td>${appointment.getTime()}</td>
                        </tr>
                    </c:forEach>
                </table>
                <br>
                <table style="margin:0 auto">
                    <tr>
                        <td align="center" colspan="2"><input class="btn_view" type="submit" value="View"></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>

