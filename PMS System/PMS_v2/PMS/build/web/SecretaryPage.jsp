<%-- 
    Document   : SecretaryPage
    Created on : 2020年1月15日, 下午02:50:53
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            .navbar {
                overflow: hidden;
                background-color: #7892c2;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover {
                background-color: #476e9e;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC">
        <div class="navbar">
            <a href="./secretary/ManageAppointment.jsp" >Create appointment</a>
            <a href="./secretary/RemovePatients.jsp" >Remove patients</a>
            <a href="./" >Logout</a>
        </div>
    </body>
</html>

