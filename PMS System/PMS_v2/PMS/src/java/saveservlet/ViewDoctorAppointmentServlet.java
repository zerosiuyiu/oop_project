/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.io.PrintWriter;
import main.Appointment;
import java.util.ArrayList;
import java.util.List;
import main.Doctor;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "ViewDoctorAppointmentServlet", urlPatterns = "/ViewDoctorAppointmentServlet")
public class ViewDoctorAppointmentServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String doctorID = request.getParameter("id");
        PrintWriter out = response.getWriter();
        int chkDoctor = 0;

        for (Doctor doctor : ManageData.doctorList) {
            if (doctorID.equals(doctor.getId())) {
                chkDoctor = 1;
                break;
            }
        }

        if (chkDoctor == 1) {
            List<Appointment> appointment = new ArrayList<>();
            for (Appointment app : ManageData.appointmentsList) {
                if (doctorID.equals(app.getDoctorid())) {
                    appointment.add(app);
                }
            }
            request.setAttribute("list", appointment);
            request.getRequestDispatcher("/doctor/ViewAppointments.jsp").forward(request, response);
        } else {
            out.println("<script language='javascript'>");
            out.println("var str='No this doctor id';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/ViewAppointments.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
