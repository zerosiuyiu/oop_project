/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.io.PrintWriter;
import main.Patient;
import main.Prescription;
import java.util.ArrayList;
import java.util.List;
import main.Appointment;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "ViewPrescriptionServlet", urlPatterns = "/ViewPrescriptionServlet")
public class ViewPrescriptionServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");
        PrintWriter out = response.getWriter();
        int chkPatient = 0;

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                chkPatient = 1;
                break;
            }
        }

        if (chkPatient == 1) {
            List<Prescription> prescription = new ArrayList<>();

            for (Patient patient : ManageData.patientList) {
                if (patientID.equals(patient.getId())) {
                    prescription.add(patient.getPrescription());
                }
            }
            request.setAttribute("list", prescription);
            request.getRequestDispatcher("/patient/ViewPrescription.jsp").forward(request, response);
        } else {
            out.println("<script language='javascript'>");
            out.println("var str='No this patient id';");
            out.println("alert(str);");
            out.println("window.location.href='./patient/ViewPrescription.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
