/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import designpattern.MessageServer;
import designpattern.Observer;
import main.Appointment;
import main.Doctor;
import main.Patient;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "ManageAppointmentServlet", urlPatterns = "/ManageAppointmentServlet")
public class ManageAppointmentServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("patientID");
        String doctorID = request.getParameter("doctorID");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        PrintWriter out = response.getWriter();
        MessageServer messageServer = new MessageServer();
        Observer patientObserver = null;
        Observer doctorObserver = null;
        int checkPatient = 0;
        int checkDoctor = 0;
        
        for (Patient patient : ManageData.patientList) {
            if (patient.getId().equals(patientID)) {
                patientObserver = patient;
                checkPatient = 1;
                break;
            }
        }
        for (Doctor doctor : ManageData.doctorList) {
            if (doctor.getId().equals(doctorID)) {
                doctorObserver = doctor;
                checkDoctor = 1;
                break;
            }
        }
        
        if (checkPatient == 1 && checkDoctor == 1) {
            Appointment appointment = new Appointment(patientID, doctorID, date, time);
            ManageData.appointmentsList.add(appointment);
            messageServer.registerObserber(patientObserver);
            messageServer.registerObserber(doctorObserver);
            messageServer.setMessageAndNotifyAll(appointment);

            out.println("<script language='javascript'>");
            out.println("var str='Submit appointment success';");
            out.println("alert(str);");
            out.println("window.location.href='./secretary/ManageAppointment.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else if (checkPatient == 0 && checkDoctor == 1) {
            out.println("<script language='javascript'>");
            out.println("var str='Submit appointment fail. Cannot find this patient';");
            out.println("alert(str);");
            out.println("window.location.href='./secretary/ManageAppointment.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else if (checkPatient == 1 && checkDoctor == 0) {
            out.println("<script language='javascript'>");
            out.println("var str='Submit appointment fail. Cannot find this doctor';");
            out.println("alert(str);");
            out.println("window.location.href='./secretary/ManageAppointment.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else {
            out.println("<script language='javascript'>");
            out.println("var str='Submit appointment fail. Cannot find this doctor and patient';");
            out.println("alert(str);");
            out.println("window.location.href='./secretary/ManageAppointment.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
