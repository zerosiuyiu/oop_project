/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import main.Patient;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "TerminatePatientAccountServlet", urlPatterns = "/TerminatePatientAccountServlet")
public class TerminatePatientServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");
        PrintWriter out = response.getWriter();

        for (Patient pat : ManageData.patientList) {
            if (patientID.equals(pat.getId())) {
                dataOperator.removePatient(patientID);
                out.println("<script language='javascript'>");
                out.println("var str='Terminate patient success';");
                out.println("alert(str);");
                out.println("window.location.href='./secretary/RemovePatients.jsp';");
                out.println("</script>");
                out.flush();
                out.close();
                return;
            }
        }
        
        out.println("<script language='javascript'>");
        out.println("var str='Terminate patient fail. Cannot find this patient';");
        out.println("alert(str);");
        out.println("window.location.href='./patient/TerminateAccount.jsp';");
        out.println("</script>");
        out.flush();
        out.close();
    }
}
