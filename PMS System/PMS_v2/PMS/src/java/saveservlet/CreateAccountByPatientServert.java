/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.util.ArrayList;
import java.util.List;
import main.Patient;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "CreateAccountByPatientServlet", urlPatterns = "/CreateAccountByPatientServlet") // 注释

public class CreateAccountByPatientServert extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String patientID = request.getParameter("id");
        String patientPwd = request.getParameter("pwd");
        int patientAge = Integer.valueOf(request.getParameter("age"));
        String patientAddress = request.getParameter("address");
        String patientGender = request.getParameter("gender");
        int chk = 0;
        PrintWriter out = response.getWriter();

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                chk = 1;
                break;
            }
        }

        if (chk == 0) {
            ManageData.patientList.add(new Patient(patientID, patientPwd, patientAge, patientGender, patientAddress));
            out.println("<script language='javascript'>");
            out.println("var str='Create patient account success';");
            out.println("alert(str);");
            out.println("window.location.href='./patient/CreateAccount.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else {
            out.println("<script language='javascript'>");
            out.println("var str='This patient id already exist';");
            out.println("alert(str);");
            out.println("window.location.href='./patient/CreateAccount.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
