/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.io.PrintWriter;
import main.Patient;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "InspectHistoryServlet", urlPatterns = "/InspectHistoryServlet")
public class InspectHistoryServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");
        PrintWriter out = response.getWriter();
        int chk = 0;
        
        List<Patient> patients = new ArrayList<>();

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                patients.add(patient);
                chk = 1;
            }
        }
        
        if (chk == 0) {
            out.println("<script language='javascript'>");
            out.println("var str='No this patient record(s)';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/InspectPatientHistory.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
            return;
        }
        
        request.setAttribute("doctorList", patients);
        request.getRequestDispatcher("/doctor/InspectPatientHistory.jsp").forward(request, response);
    }
}
