/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Patient extends User {

    private int age;
    private String gender;
    private String address;
    private String role;
    private String history;
    private Appointment appointment;
    private Prescription prescription;

    /*
     *
     * @param id
     * @param pwd
     */
    public Patient(String id, String pwd) {
        super(id, pwd);
        role = "patient";
    }

    /*
     *
     * @param id
     * @param pwd
     * @param age
     * @param gender
     * @param address
     */
    public Patient(String id, String pwd, int age, String gender, String address) {
        super(id, pwd);
        this.age = age;
        this.gender = gender;
        this.address = address;
        role = "patient";
    }

    /*
     *
     * @return
     */
    public String getRole() {
        return role;
    }

    /*
     *
     * @return
     */
    public String getHistory() {
        return history;
    }

    /*
     *
     * @param history
     */
    public void setHistory(String history) {
        this.history = history;
    }

    /*
     *
     * @return
     */
    public Appointment getAppointment() {
        return appointment;
    }

    /*
     *
     * @param appointment
     */
    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    /*
     *
     * @return
     */
    public Prescription getPrescription() {
        return prescription;
    }

    /*
     *
     * @param prescription
     */
    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    /*
     *
     * @return
     */
    public int getAge() {
        return age;
    }

    /*
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /*
     *
     * @return
     */
    public String getGender() {
        return gender;
    }

    /*
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /*
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /*
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /*
     *
     * @param appointment
     */
    @Override
    public void update(Appointment appointment) {
        if (getId().equals(appointment.getPatientid())) {
            this.appointment = appointment;
        }
    }
}
