/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Appointment {

    private String patientid;
    private String doctorid;
    private String date;
    private String time;

    /*
     *
     * @param patientId
     * @param doctorId
     * @param date
     * @param time
     */
    public Appointment(String patientId, String doctorId, String date, String time) {
        this.patientid = patientId;
        this.doctorid = doctorId;
        this.date = date;
        this.time = time;
    }

    /*
     *
     * @return
     */
    public String getPatientid() {
        return patientid;
    }

    /*
     *
     * @param patientid
     */
    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    /*
     *
     * @return
     */
    public String getDoctorid() {
        return doctorid;
    }

    /*
     *
     * @param doctorid
     */
    public void setDoctorid(String doctorid) {
        this.doctorid = doctorid;
    }

    /*
     *
     * @return
     */
    public String getDate() {
        return date;
    }

    /*
     *
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }
    
    /*
     *
     * @return
     */
    public String getTime() {
        return time;
    }

    /*
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }
}
