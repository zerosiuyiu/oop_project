/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author ZeroSiuYiu
 */
public class DaoFactory {

    private volatile static DataOperator dataOperator;

    private DaoFactory() {
    }

    /*
     *
     * @return
     */
    public static DataOperator getSingleDataOperator() {
        if (dataOperator == null) {
            synchronized (DaoFactory.class) {
                if (dataOperator == null) {
                    dataOperator = new DataOperator();
                }
            }
        }
        return dataOperator;
    }
}
