/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.LinkedList;
import java.util.List;
import main.*;

/**
 *
 * @author ZeroSiuYiu
 */
public class ManageData {

    /*
     *
     */
    public static List<Patient> patientList = new LinkedList<>();

    /*
     *
     */
    public static List<Doctor> doctorList = new LinkedList<>();

    /*
     *
     */
    public static List<Secretary> secretaryList = new LinkedList<>();

    /*
     *
     */
    public static List<Administrator> administratorList = new LinkedList<>();

    /*
     *
     */
    public static List<Appointment> appointmentsList = new LinkedList<>();

    /**
     *
     */
    public static List<Prescription> prescriptionsList = new LinkedList<>();

    /*
     *
     */
    public void init() {

        Patient john = new Patient("P0001", "John");
        john.setAddress("Portland Square Building, Drake Circus, Plymouth, PL4 8AA");
        john.setGender("Male");
        john.setAge(45);
        john.setHistory("no other sicks!!!!");

        Patient eric = new Patient("P0002", "Eric");
        Patient anson = new Patient("P0003", "Anson");

        Doctor james = new Doctor("D0001", "James");
        james.setAddress("Fictitious Clinic, Diagon Alley, Drake Circus, Plymouth, PL4 8AA");
        Appointment appointment = new Appointment("P0001", "D0001", "2019-1-1", "11:00");

        //Doctor kobe = new Doctor("D1002", "Kobe");
        //Doctor curry = new Doctor("D1003", "Curry");

        String medicine = "Amoxicillin";
        int quantity = 24;
        String dosage = "4 per day – at least 6 hours between each dose";
        Prescription prescription = new Prescription(medicine, quantity, dosage);
        prescription.setDoctorID("D0001");
        prescription.setPatientID("P0001");
        john.setPrescription(prescription);
        john.setAppointment(appointment);
        james.setAppointment(appointment);

        Administrator admin = new Administrator("A0001", "admin");
        Secretary secretary = new Secretary("S0001", "secret");

        ManageData.patientList.add(john);
        ManageData.patientList.add(eric);
        ManageData.patientList.add(anson);
        ManageData.doctorList.add(james);
        //ManageData.doctorList.add(kobe);
        //ManageData.doctorList.add(curry);
        ManageData.appointmentsList.add(appointment);
        ManageData.prescriptionsList.add(prescription);
        ManageData.administratorList.add(admin);
        ManageData.secretaryList.add(secretary);
    }
}
