/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern;

import main.Appointment;

/**
 *
 * @author ZeroSiuYiu
 */

public interface Observer {

    /*
     *
     * @param appointment
     */
    public void update(Appointment appointment);
}
