<%-- 
    Document   : RemoveUser
    Created on : 2020年1月15日, 下午02:54:38
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Remove Account</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }
            
            .navbar {
                overflow: hidden;
                background-color: #7892c2;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: #476e9e;
            }

            .dropdown {
                float: left;
                overflow: hidden;
            }

            .dropdown .dropbtn {
                font-size: 16px;  
                border: none;
                outline: none;
                color: white;
                padding: 14px 16px;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }

            .dropdown-content a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: left;
            }

            .dropdown-content a:hover {
                background-color: #ddd;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }
            
            .btn_remove {
                display: inline-block;
                padding: 5px 60px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;   
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #7892c2;
                border: none;
                border-radius: 15px;
                box-shadow: 0 9px #999;
            }

            .btn_remove:hover {background-color: #476e9e}

            .btn_remove:active {
                background-color: #476e9e;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }
            
            th {
                background-color: #5a97e8;
                color: #fff;
                border-bottom-width: 0;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC" >
        <div class="navbar">
            <div class="dropdown">
                <button class="dropbtn">Account</button>
                <div class="dropdown-content">
                    <a href="./CreateAccount.jsp" >Create account</a>
                    <a href="./RemoveUser.jsp">Remove accounts</a>
                </div>
            </div> 
            <a href="./ViewDoctorsRatings.jsp" >View Doctors Rate</a>
            <a href="../" >Logout</a>
        </div>
        <div>
            <br>
            <form name="remove_user_from" action="../RemoveUserServlet" method="post" target="_self">       
                <table border="1" align="center" style="margin:0 auto">
                    <tr>
                        <th align="center" colspan="2" style="background-color: #5b6cff;">
                            <h3>Remove User</h3>
                        </th>
                    </tr>     
                    <tr>
                        <th>Login ID</th>
                        <td><input type="text" name="userID" /></td>				
                    </tr>  
                </table>
                <br>
                <table style="margin:0 auto">
                    <tr>
                        <td align="center" colspan="2">
                            <input class="btn_remove" type="submit" value="Remove">
                        </td>				
                    </tr>           
                </table>
            </form>
        </div>
    </body>
</html>

