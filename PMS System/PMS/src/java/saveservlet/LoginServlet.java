/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String username = request.getParameter("username");
        String pwd = request.getParameter("pwd");

        new ManageData().init();

        if (dataOperator.vaildUser(username.substring(0, 1), username, pwd)) {
            if (username.substring(0, 1).equals("P")) {
                request.getRequestDispatcher("/PatientPage.jsp").forward(request, response);
            } else if (username.substring(0, 1).equals("A")) {
                request.getRequestDispatcher("/AdministratorPage.jsp").forward(request, response);
            } else if (username.substring(0, 1).equals("S")) {
                request.getRequestDispatcher("/SecretaryPage.jsp").forward(request, response);
            } else if (username.substring(0, 1).equals("D")) {
                request.getRequestDispatcher("/DoctorPage.jsp").forward(request, response);
            }
        } else {
            PrintWriter out = response.getWriter();
            out.println("<script language='javascript'>");
            out.println("var str='Input is incorrect! please input again!';");
            out.println("alert(str);");
            out.println("window.location.href='index.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
