/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import main.Patient;
import main.Prescription;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "PrescribeMedicinesServlet", urlPatterns = "/PrescribeMedicinesServlet")
public class PrescribeMedicinesServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("patientID");
        String medicine = request.getParameter("medicine");
        int quantity = Integer.valueOf(request.getParameter("quantity"));
        String dosage = request.getParameter("dosage");
        String doctorID = request.getParameter("doctorID");

        Prescription prescription = new Prescription(medicine, quantity, dosage, doctorID, patientID);

        ManageData.prescriptionsList.add(prescription);
        for (Patient patient : ManageData.patientList) {
            if (patient.getId().equals(patientID)) {
                patient.setPrescription(prescription);
                patient.setHistory("Got the Prescription, the Medicine is : " + prescription.getMdeicine());
            }
        }

        PrintWriter out = response.getWriter();
        out.println("<script language='javascript'>");
        out.println("var str='Prescribe Medicines Success ！';");
        out.println("alert(str);");
        out.println("window.location.href='./doctor/PrescribeMedicinesAndDosages.jsp';");
        out.println("</script>");
        out.flush();
        out.close();
    }
}
