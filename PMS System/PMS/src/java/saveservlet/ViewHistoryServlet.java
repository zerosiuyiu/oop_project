/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.util.ArrayList;
import java.util.List;
import main.Patient;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "ViewHistoryServlet", urlPatterns = "/ViewHistoryServlet")
public class ViewHistoryServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");

        List<Patient> p = new ArrayList<>();

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                p.add(patient);
            }
        }
        request.setAttribute("patientList", p);
        request.getRequestDispatcher("/patient/ViewHistory.jsp").forward(request, response);
    }
}
