/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "TerminatePatientAccountServlet", urlPatterns = "/TerminatePatientAccountServlet")
public class TerminatePatientServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");
        String patientPwd = request.getParameter("pwd");

        dataOperator.removePatient(patientID);

        PrintWriter out = response.getWriter();
        out.println("<script language='javascript'>");
        out.println("var str=' Terminate Patient Success ！! !';");
        out.println("alert(str);");
        out.println("window.location.href='PatientPage.jsp';");
        out.println("</script>");
        out.flush();
        out.close();

    }
}
