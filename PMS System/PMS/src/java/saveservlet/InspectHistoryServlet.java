/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import main.Patient;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "InspectHistoryServlet", urlPatterns = "/InspectHistoryServlet")
public class InspectHistoryServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("id");

        List<Patient> patients = new ArrayList<>();

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                patients.add(patient);
            }
        }
        request.setAttribute("doctorList", patients);
        request.getRequestDispatcher("/doctor/InspectPatientHistory.jsp").forward(request, response);
    }
}
