/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern;

/**
 *
 * @author ZeroSiuYiu
 */
public interface Observable {

    public void registerObserber(Observer observer);

    public void removeObserver(Observer observer);

    public void notifyObserver();
}
