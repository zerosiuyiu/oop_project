/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Iterator;
import main.*;

/**
 *
 * @author ZeroSiuYiu
 */
public class DataOperator {

    public boolean vaildUser(String type, String id, String pwd) {
        if (type.equals("P")) {
            for (Patient patient : ManageData.patientList) {
                if (patient.getId().equals(id) && patient.getPwd().equals(pwd)) {
                    return true;
                }
            }
            return false;
        } else if (type.equals("D")) {
            for (Doctor doctor : ManageData.doctorList) {
                if (doctor.getId().equals(id) && doctor.getPwd().equals(pwd)) {
                    return true;
                }
            }
            return false;
        } else if (type.equals("A")) {
            for (Administrator administrator : ManageData.administratorList) {
                if (administrator.getId().equals(id) && administrator.getPwd().equals(pwd)) {
                    return true;
                }
            }
            return false;
        } else if (type.equals("S")) {
            for (Secretary secretary : ManageData.secretaryList) {
                if (secretary.getId().equals(id) && secretary.getPwd().equals(pwd)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public void removePatient(String patientID) {
        Iterator<Patient> iterable = ManageData.patientList.iterator();
        while (iterable.hasNext()) {
            Patient patient = iterable.next();
            if (patientID.equals(patient.getId())) {
                iterable.remove();
            }
        }
    }

    public boolean removeUser(String userID, String userType) {
        boolean check = false;
        
        if ("P".equals(userType)) {
            Iterator<Patient> iterable = ManageData.patientList.iterator();
            while (iterable.hasNext()) {
                Patient patient = iterable.next();
                if (userID.equals(patient.getId())) {
                    iterable.remove();
                    check = true;
                }
            }
        } else if ("D".equals(userType)) {
            Iterator<Doctor> iterable = ManageData.doctorList.iterator();
            while (iterable.hasNext()) {
                Doctor doctor = iterable.next();
                if (userID.equals(doctor.getId())) {
                    iterable.remove();
                    check = true;
                }
            }
        } else if ("A".equals(userType)) {
            Iterator<Administrator> iterable = ManageData.administratorList.iterator();
            while (iterable.hasNext()) {
                Administrator administrator = iterable.next();
                if (userID.equals(administrator.getId())) {
                    iterable.remove();
                    check = true;
                }
            }
        } else if ("S".equals(userType)) {
            Iterator<Secretary> iterable = ManageData.secretaryList.iterator();
            while (iterable.hasNext()) {
                Secretary secretary = iterable.next();
                if (userID.equals(secretary.getId())) {
                    iterable.remove();
                    check = true;
                }
            }
        }
        return check;
    }
}
