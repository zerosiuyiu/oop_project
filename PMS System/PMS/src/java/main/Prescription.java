/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Prescription {

    private String medicine;
    private int quantity;
    private String dosage;
    private String doctorID;
    private String patientID;

    public Prescription(String mdeicine, int quantity, String dosage) {
        super();
        this.medicine = mdeicine;
        this.quantity = quantity;
        this.dosage = dosage;
    }

    public Prescription(String mdeicine, int quantity, String dosage, String doctorID, String patientID) {
        super();
        this.medicine = mdeicine;
        this.quantity = quantity;
        this.dosage = dosage;
        this.doctorID = doctorID;
        this.patientID = patientID;
    }

    public String getMdeicine() {
        return medicine;
    }

    public void setMdeicine(String mdeicine) {
        this.medicine = mdeicine;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }
}
