/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Doctor extends User {

    private String role;
    private String ratings;
    private Appointment appointment;
    private String address;

    public Doctor(String id, String pwd) {
        super(id, pwd);
        role = "doctor";
    }

    public String getRole() {
        return role;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void update(Appointment appointment) {
        if (getId().equals(appointment.getDoctorid())) {
            this.appointment = appointment;
        }
    }
}
