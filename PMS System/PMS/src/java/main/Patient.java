/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Patient extends User {

    private int age;
    private String gender;
    private String address;
    private String role;
    private String history;
    private Appointment appointment;
    private Prescription prescription;

    public Patient(String id, String pwd) {
        super(id, pwd);
        role = "patient";
    }

    public Patient(String id, String pwd, int age, String gender, String address) {
        super(id, pwd);
        this.age = age;
        this.gender = gender;
        this.address = address;
        role = "patient";
    }

    public String getRole() {
        return role;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void update(Appointment appointment) {
        if (getId().equals(appointment.getPatientid())) {
            this.appointment = appointment;
        }
    }
}
