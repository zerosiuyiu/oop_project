/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Secretary extends User {

    private String role;

    public Secretary(String id, String pwd) {
        super(id, pwd);
        role = "secretary";
    }

    public String getRole() {
        return role;
    }
}
