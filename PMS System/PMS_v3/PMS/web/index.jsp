<%-- 
    Document   : index
    Created on : 2020年1月15日, 下午12:04:45
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=password], select {
                width: 100%;
                padding: 5px 20px;
                margin: 2px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            .btn_grey {
                display: inline-block;
                padding: 5px 60px;
                font-size: 24px;
                cursor: pointer;
                text-align: center;   
                text-decoration: none;
                outline: none;
                color: #fff;
                background-color: #7892c2;
                border: none;
                border-radius: 15px;
                box-shadow: 0 9px #999;
            }

            .btn_grey:hover {background-color: #476e9e}

            .btn_grey:active {
                background-color: #476e9e;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
            }

            th {
                background-color: #5a97e8;
                color: #fff;
                border-bottom-width: 0;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC">
        <form action="LoginServlet" method="post" name="form1">
            <div style="text-align: center;">
                <h1>Patient Management System</h1>
            </div>
            <table border="1" style="margin:0 auto">
                <tr>
                    <th>Login ID</th>
                    <td><input name="username" type="text" maxlength="20"></td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td><input name="pwd" type="password" size="20" maxlength="20"></td>
                </tr>
            </table >
            <br>
            <table style="margin:0 auto">
                <tr>
                    <td colspan="2"><input name="Submit" type="submit" class="btn_grey" value="Login" style="width: 100%"></td>
                </tr>
            </table>
        </form>
    </body>
</html>

