/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern;

import java.util.ArrayList;
import java.util.List;
import main.Appointment;

/**
 *
 * @author ZeroSiuYiu
 */
public class MessageServer implements Observable {

    private List<Observer> observersList;
    private Appointment appointment;

    /*
     *
     */
    public MessageServer() {
        observersList = new ArrayList<>();
    }

    /*
     *
     * @param observer
     */
    @Override
    public void registerObserber(Observer observer) {
        observersList.add(observer);
    }

    /*
     *
     * @param observer
     */
    @Override
    public void removeObserver(Observer observer) {
        if (!observersList.isEmpty()) {
            observersList.remove(observer);
        }
    }

    /*
     *
     */
    @Override
    public void notifyObserver() {
        for (Observer observer : observersList) {
            observer.update(appointment);
        }
    }

    /*
     *
     * @param appointment
     */
    public void setMessageAndNotifyAll(Appointment appointment) {
        this.appointment = appointment;
        notifyObserver();
    }
}
