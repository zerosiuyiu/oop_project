/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import java.util.ArrayList;
import java.util.List;
import main.Doctor;
import main.Patient;
import main.Prescription;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "PrescribeMedicinesServlet", urlPatterns = "/PrescribeMedicinesServlet")
public class PrescribeMedicinesServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String patientID = request.getParameter("patientID");
        String medicine = request.getParameter("medicine");
        int quantity = Integer.valueOf(request.getParameter("quantity"));
        String dosage = request.getParameter("dosage");
        String doctorID = request.getParameter("doctorID");
        PrintWriter out = response.getWriter();
        int chkPatient = 0;
        int chkDoctor = 0;

        for (Patient patient : ManageData.patientList) {
            if (patientID.equals(patient.getId())) {
                chkPatient = 1;
                break;
            }
        }

        for (Doctor doctor : ManageData.doctorList) {
            if (doctorID.equals(doctor.getId())) {
                chkDoctor = 1;
                break;
            }
        }

        if (chkPatient == 0 && chkDoctor == 1) {
            out.println("<script language='javascript'>");
            out.println("var str='No this patient id';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/PrescribeMedicinesAndDosages.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else if (chkPatient == 1 && chkDoctor == 0) {
            out.println("<script language='javascript'>");
            out.println("var str='No this doctor id';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/PrescribeMedicinesAndDosages.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else if (chkPatient == 0 && chkDoctor == 0) {
            out.println("<script language='javascript'>");
            out.println("var str='No this patient and doctor id';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/PrescribeMedicinesAndDosages.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        } else {
            Prescription prescription = new Prescription(medicine, quantity, dosage, doctorID, patientID);

            ManageData.prescriptionsList.add(prescription);
            for (Patient patient : ManageData.patientList) {
                if (patient.getId().equals(patientID)) {
                    patient.setPrescription(prescription);
                    patient.setHistory("Prescription ->  Medicine : " + prescription.getMedicine());
                }
            }

            out.println("<script language='javascript'>");
            out.println("var str='Prescribe medicines success';");
            out.println("alert(str);");
            out.println("window.location.href='./doctor/PrescribeMedicinesAndDosages.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
        }
    }
}
