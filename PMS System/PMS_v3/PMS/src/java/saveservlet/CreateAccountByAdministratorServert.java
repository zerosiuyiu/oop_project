/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saveservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import main.Administrator;
import main.Doctor;
import main.Patient;
import main.Secretary;

/**
 *
 * @author ZeroSiuYiu
 */
@WebServlet(name = "CreateAccountByAdministratorServlet", urlPatterns = "/CreateAccountByAdministratorServlet") // 注释
public class CreateAccountByAdministratorServert extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DataOperator dataOperator = null;

    /*
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
        dataOperator = DaoFactory.getSingleDataOperator();
    }

    /*
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String accountID = request.getParameter("id");
        String accountPwd = request.getParameter("pwd");
        String accountType = request.getParameter("UserSort");
        PrintWriter out = response.getWriter();

        if (accountID == null) {
            out.println("<script language='javascript'>");
            out.println("var str='Please input the login ID';");
            out.println("alert(str);");
            out.println("window.location.href='./administrator/CreateAccount.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
            return;
        }
        
        if (accountPwd == null) {
            out.println("<script language='javascript'>");
            out.println("var str='Please input the password';");
            out.println("alert(str);");
            out.println("window.location.href='./administrator/CreateAccount.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
            return;
        }

        if (accountType.equals("Patient") && accountID.substring(0, 1).equals("P")) {
            ManageData.patientList.add(new Patient(accountID, accountPwd));
        } else if (accountType.equals("Doctor") && accountID.substring(0, 1).equals("D")) {
            ManageData.doctorList.add(new Doctor(accountID, accountPwd));
        } else if (accountType.equals("Administrator") && accountID.substring(0, 1).equals("A")) {
            ManageData.administratorList.add(new Administrator(accountID, accountPwd));
        } else if (accountType.equals("Secretary") && accountID.substring(0, 1).equals("S")) {
            ManageData.secretaryList.add(new Secretary(accountID, accountPwd));
        } else {
            out.println("<script language='javascript'>");
            out.println("var str='First character of login ID must same with the role first character';");
            out.println("alert(str);");
            out.println("window.location.href='./administrator/CreateAccount.jsp';");
            out.println("</script>");
            out.flush();
            out.close();
            return;
        }

        out.println("<script language='javascript'>");
        out.println("var str='Create account success';");
        out.println("alert(str);");
        out.println("window.location.href='./administrator/CreateAccount.jsp';");
        out.println("</script>");
        out.flush();
        out.close();
    }
}
