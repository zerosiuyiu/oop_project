/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Prescription {

    private String medicine;
    private int quantity;
    private String dosage;
    private String doctorID;
    private String patientID;

    /*
     *
     * @param mdeicine
     * @param quantity
     * @param dosage
     */
    public Prescription(String mdeicine, int quantity, String dosage) {
        super();
        this.medicine = mdeicine;
        this.quantity = quantity;
        this.dosage = dosage;
    }

    /*
     *
     * @param mdeicine
     * @param quantity
     * @param dosage
     * @param doctorID
     * @param patientID
     */
    public Prescription(String mdeicine, int quantity, String dosage, String doctorID, String patientID) {
        super();
        this.medicine = mdeicine;
        this.quantity = quantity;
        this.dosage = dosage;
        this.doctorID = doctorID;
        this.patientID = patientID;
    }

    /*
     *
     * @return
     */
    public String getMedicine() {
        return medicine;
    }

    /*
     *
     * @param mdeicine
     */
    public void setMedicine(String mdeicine) {
        this.medicine = mdeicine;
    }

    /*
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /*
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /*
     *
     * @return
     */
    public String getDosage() {
        return dosage;
    }

    /*
     *
     * @param dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    /*
     *
     * @return
     */
    public String getDoctorID() {
        return doctorID;
    }

    /*
     *
     * @param doctorID
     */
    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    /*
     *
     * @return
     */
    public String getPatientID() {
        return patientID;
    }

    /*
     *
     * @param patientID
     */
    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }
}
