/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ZeroSiuYiu
 */
public class Administrator extends User {

    private String role;

    /*
     *
     * @param id
     * @param pwd
     */
    public Administrator(String id, String pwd) {
        super(id, pwd);
        role = "administrator";
    }

    /*
     *
     * @return
     */
    public String getRole() {
        return role;
    }
}
