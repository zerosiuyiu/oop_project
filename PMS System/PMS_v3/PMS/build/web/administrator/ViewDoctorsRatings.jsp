<%-- 
    Document   : ViewDoctorsRatings
    Created on : 2020年1月15日, 下午02:55:32
    Author     : ZeroSiuYiu
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doctors Ratings</title>
        <style>
            .navbar {
                overflow: hidden;
                background-color: #7892c2;
                font-family: Arial, Helvetica, sans-serif;
            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: #476e9e;
            }

            .dropdown {
                float: left;
                overflow: hidden;
            }

            .dropdown .dropbtn {
                font-size: 16px;  
                border: none;
                outline: none;
                color: white;
                padding: 14px 16px;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }

            .dropdown-content a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: left;
            }

            .dropdown-content a:hover {
                background-color: #ddd;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }
        </style>
    </head>
    <body bgcolor="#FFFFCC" >  
        <div class="navbar">
            <div class="dropdown">
                <button class="dropbtn">Account</button>
                <div class="dropdown-content">
                    <a href="./CreateAccount.jsp" >Create account</a>
                    <a href="./RemoveUser.jsp">Remove accounts</a>
                </div>
            </div> 
            <a href="./ViewDoctorsRatings.jsp" >View Doctors Rate</a>
            <a href="../" >Logout</a>
        </div>
        <div>
            <h3 align="center">Doctor Ratings</h3>
            <table border="1" align="center">
                <tr>
                    <td bgcolor="white">Dr. James: <b>Medicine potency is good</b></td>
                </tr>
                <tr>
                    <td bgcolor="white">Dr. Louis: <b>Explain in detail</b></td>
                </tr>
            </table>
        </div>
    </body>
</html>